FROM node:14 AS BUILD_IMAGE
WORKDIR /app
COPY ./package.json /app
EXPOSE 8080
RUN apt-get update && apt-get install -q -y \
    openssh-client
RUN npm install http-server -g
RUN npm install \
	&& git init \
	&& git config --global user.name "John Snow" && git config --global user.email "knows-nothing@winterfell.gov" \
	&& git commit --allow-empty -n -m "Startup Initial Commit"

COPY . /app
RUN npm run build
RUN npm prune --production

FROM node:14-alpine

WORKDIR /app

# copy from build image
COPY --from=BUILD_IMAGE /app/public ./dist
COPY --from=BUILD_IMAGE /app/node_modules ./node_modules

RUN npm i -g http-server

CMD http-server ./dist
